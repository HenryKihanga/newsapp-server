<?php

namespace App\Http\Controllers;

use App\Author;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AuthorController extends Controller{
    //return all authors from DB
    public function getAuthors()
    {
        $authors = Author::all(); //scope resolution operation ie frending a class or aking to use the class

        return response()->json(['authors' => $authors], 200);
    }




    //return a single author fetched by ID
    public function getAuthor($authorId)
    {

        $author = Author::find($authorId);
        if (!$author) {
            return response()->json(['error' => 'Author Not Found'], 404);
        }
        $author->albums;//relating author with album
        return response()->json(['author' => $author], 200);
    }



    //post an author to DB
    public function postAuthor(Request $request)
    {
        $validator = Validator::make($request->all(), ['name' => 'required']);
        // validator fails
        if ($validator->fails()) {
            return response()->json([
                'error' => $validator->errors(),
                'status' => false
            ], 404);
        }

        $author = new Author();
        $author->name = $request->input('name');
        $author->avatar = $request->input('avatar');


        $author->save();
        return response()->json([
            'author' => $author
        ], 201);
    }



    // edit author details in database
    public function putAuthor(Request $request, $authorId)
    {
        // validate if the request sent contains this parameters
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'avatar'=> 'required'
        ]);

        // validator fails
        if ($validator->fails()) {
            return response()->json([
                'error' => $validator->errors(),
                'status' => false
            ], 404);
        }

        //find album by id
        $author = Author::find($authorId);
        if (!$author) {
            return response()->json([
                'error' => 'Author not found'
            ], 404);
        }
        //update founded album
        $author->update([
            'name' => $request->input('name'),
            'avatar' => $request->input('avatar'),


        ]);

        return response()->json([
            'author' => $author
        ], 206);
    }



    // delete album from database
    public function deleteAuthor($authorId)
    {
        $author = Author::find($authorId);
        if (!$author) {
            return response()->json([
                'error' => 'Author not exist'
            ], 404);
        }


        $author->delete();
        return response()->json([
            'author' => 'Author deleted successfully'
        ], 200);
    }
}
