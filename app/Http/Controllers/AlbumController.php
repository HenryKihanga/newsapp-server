<?php

namespace App\Http\Controllers;

use App\Album;
use App\Author;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AlbumController extends Controller
{
    // return all albums from the database
    public function getAlbums()
    {
        $albums = Album::all();
        return response()->json([
            'albums' => $albums
        ], 200);
    }

    // return a single album
    public function getAlbum($albumId)
    {
        $album = Album::find($albumId);
        if (!$album) {
            return response()->json([
                'error' => "Album not found"
            ], 404);
        }
        return response()->json([
            'album' => $album
        ], 200);
    }

    // send album to database
    public function postAlbum(Request $request, $author_id)
    {

        $this->path =null;
        // validate if the request sent contains this parameters
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'subtitle' => 'required',
            'is_hot' => 'required',
            'details' => 'required'
        ]);

        //validate if request has file
        if ($request->hasFile('file')) {
            //obtaining the file path
            $this->path = $request->file('file')->store('albums');
        }
        // validator fails
        if ($validator->fails()) {
            return response()->json([
                'error' => $validator->errors(),
                'status' => false
            ], 404);
        }

        //checking if author exist
        $author = Author::find($author_id);
        if (!$author) {
            return response()->json(['error' => 'Author do not exist'], 404);
        }
        //creating an album
        $album = new Album();
        $album->title = $request->input('title');
        $album->subtitle = $request->input('subtitle');
        $album->is_hot = $request->input('is_hot');
        $album->details = $request->input('details');
        $album->cover = $this->path;


        //save album with coresponding author's ID
        $author->albums()->save($album);
        return response()->json([
            'album' => $album
        ], 201);
    }

    // edit album in database
    public function putAlbum(Request $request, $albumId)
    {
        // validate if the request sent contains this parameters
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'subtitle' => 'required',
            'is_hot' => 'required',
            'details' => 'required',
            'author_id' => 'required'
        ]);

        // validator fails
        if ($validator->fails()) {
            return response()->json([
                'error' => $validator->errors(),
                'status' => false
            ], 404);
        }
        //find album by id
        $album = Album::find($albumId);
        if (!$album) {
            return response()->json([
                'error' => "Album not found"
            ], 404);
        }
        //update founded album
        $album->update([
            'title' => $request->input('title'),
            'subtitle' => $request->input('subtitle'),
            'is_hot' => $request->input('is_hot'),
            'details' => $request->input('details'),
            'author_id' => $request->input('author_id'),
            'cover' => $request->input('cover'),

        ]);

        return response()->json([
            'album' => $album
        ], 206);
    }

    // delete album from database
    public function deleteAlbum($albumId)
    {
        $album = Album::find($albumId);
        if (!$album) {
            return response()->json([
                'error' => 'Album not exist'
            ], 404);
        }

        $album->delete();
        return response()->json([
            'album' => 'Album deleted successfully'
        ], 200);
    }


    //Downloading picture from the Sever
    public function viewFile($albumId)
    {
        $album = Album::find($albumId);

        if (!$album) {
            return response()->json([
                'error' => 'Album not found'
            ], 404);
        }
        $pathToFile = storage_path('/app/' . $album->cover);
        return response()->download($pathToFile);
    }
}
